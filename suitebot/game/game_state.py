from typing import Iterable, Dict, Tuple, Optional, FrozenSet

from suitebot.game.direction import LEFT, UP, DOWN, RIGHT
from suitebot.game.point import Point


class GameState:
    def __init__(self,
                 plan_width: int,
                 plan_height: int,
                 bot_ids: Iterable[int],
                 bot_location_map: Dict[int, Point],
                 bot_energy_map: Dict[int, int],
                 live_bot_ids: Iterable[int] = (),
                 obstacles: Iterable[Point] = (),
                 treasures: Iterable[Point] = (),
                 batteries: Iterable[Point] = ()):
        self._plan_width = plan_width
        self._plan_height = plan_height
        self._bot_ids = tuple(bot_ids)
        self._live_bot_ids = frozenset(live_bot_ids if live_bot_ids else bot_ids)
        self._bot_location_map = dict(bot_location_map.items())
        self._bot_energy_map = dict(bot_energy_map.items())
        self._obstacles = frozenset(obstacles)
        self._treasures = frozenset(treasures)
        self._batteries = frozenset(batteries)

    def get_plan_width(self) -> int:
        """Returns the width of the game plan.

        :return the width of the game plan
        """
        return self._plan_width

    def get_plan_height(self) -> int:
        """Returns the height of the game plan.

        :return the height  of the game plan
        """
        return self._plan_height

    def get_all_bot_ids(self) -> Tuple[int]:
        """Returns the list of the IDs of all bots, including the dead ones.

        :return the list of the IDs of all bots
        """
        return self._bot_ids

    def get_live_bot_ids(self) -> FrozenSet[int]:
        """Returns the set of the IDS of all live bots, i.e. the bots that are still active in the game.

        :return the set of the IDS of all live bots
        """
        return self._live_bot_ids

    def get_bot_location(self, bot_id: int) -> Optional[Point]:
        """Returns the coordinates of the location of the bot on the game plan.

        :param bot_id: ID of the bot
        :return the location of the bot or None if the bot is dead
        :raises ValueError: if the bot ID is unknown
        """
        self._assert_known_bot(bot_id)
        return self._bot_location_map.get(bot_id)

    def get_all_bot_locations(self, me: int):
        locations = set(self._bot_location_map.values())
        locations.remove(self._bot_location_map.get(me))
        return locations

    def get_bot_threats(self, me: int):
        s = set()
        for bot in self.get_all_bot_locations(me):
            s2 = {bot, LEFT.destination_from(bot), RIGHT.destination_from(bot), UP.destination_from(bot),
                  DOWN.destination_from(bot)}
            s = s.union(s2)

        return s

    def get_bot_energy(self, bot_id: int) -> int:
        """Returns current energy of the bot. The energy is a non-negative number.

        :param bot_id: ID of the bot
        :return current energy of the bot
        :raises ValueError: if the bot ID is unknown
        """
        self._assert_known_bot(bot_id)
        return self._bot_energy_map[bot_id]

    def get_obstacle_locations(self) -> FrozenSet[Point]:
        """Returns the set of coordinates of all obstacles on the game plan.

        :return the set of coordinates of all obstacles
        """
        return self._obstacles

    def get_treasure_locations(self) -> FrozenSet[Point]:
        """Returns the set of coordinates of all treasures on the game plan.

        :return the set of coordinates of all treasures
        """
        return self._treasures

    def get_battery_locations(self) -> FrozenSet[Point]:
        """Returns the set of coordinates of all batteries on the game plan.

        :return the set of coordinates of all batteries
        """
        return self._batteries

    def normalize_point(self, point: Point) -> Point:
        x = point.x
        y = point.y

        if x < 0:
            x += self.get_plan_width()
        if x >= self.get_plan_width():
            x -= self.get_plan_width()
        if y < 0:
            y += self.get_plan_height()
        if y >= self.get_plan_height():
            y -= self.get_plan_height()

        return Point(x, y)

    def _assert_known_bot(self, bot_id: int) -> None:
        if bot_id not in self._bot_ids:
            raise ValueError("uknown bot ID: %i" % bot_id)
