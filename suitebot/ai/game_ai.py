from suitebot.game.direction import LEFT, UP, RIGHT, DOWN, Direction
from .bot_ai import BotAi
from ..game.game_state import GameState
from ..game.move import Move
from typing import Optional
from math import floor

def get_position(game_state: GameState, bot_id: int):
    return game_state.get_bot_location(bot_id)

class AI(BotAi):
    def __init__(self):
        self._game_state = None
        self._bot_id = None

        self.current_direction = RIGHT
        self.vertical_direction = UP

    def get_name(self):
        return 'cleverbotname'

    @property
    def game_state(self):
        return self._game_state

    @game_state.setter
    def game_state(self, new_state):
        self._game_state = new_state

    @property
    def bot_id(self):
        return self._bot_id

    @bot_id.setter
    def bot_id(self, value):
        self._bot_id = value

    @property
    def position(self):
        return self.game_state.get_bot_location(self.bot_id)

    def make_move(self, bot_id: int, game_state: GameState) -> Optional[Move]:
        self.bot_id = bot_id
        self.game_state = game_state

        prospective_move = self.spiral()

        return self.just_dont_die(prospective_move)
    
    def is_blocked(self, direction: Direction):
        obstacles = self.game_state.get_obstacle_locations().union(self._game_state.get_all_bot_locations(self._bot_id))
        point_to_look = self.game_state.normalize_point(direction.destination_from(self.position))
        return point_to_look in obstacles

    def just_dont_die(self, prospective_move) -> Optional[Move]:

        moves = [LEFT, UP, RIGHT, DOWN]
        if prospective_move is not None:
            moves.insert(0, prospective_move.step1)

        non_blocked_moves = [move for move in moves if not self.is_blocked(move)]
        non_threat_moves = [move for move in non_blocked_moves if not self.is_threat(move)]

        if len(non_threat_moves) > 0:
            return Move(non_threat_moves[0])
        return Move(non_blocked_moves[0])

    def spiral(self):
        if self.current_direction == RIGHT:
            self.vertical_direction = self.get_opposite_direction(self.vertical_direction)
            return Move(self.vertical_direction)

        position = self.game_state.get_bot_location(self.bot_id)
        if position.y == 0:
            self.current_direction = RIGHT
            self.vertical_direction = UP
        if position.y == (self.game_state.get_plan_height() - 1):
            self.current_direction = RIGHT
            self.vertical_direction = DOWN

        return Move(self.current_direction)

    def is_threat(self, direction):
        threats = self.game_state.get_bot_threats(self._bot_id)
        point_to_look = self.game_state.normalize_point(direction.destination_from(self.position))
        return point_to_look in threats

    @staticmethod
    def get_opposite_direction(direction):
        if direction == UP:
            return DOWN
        else:
            return UP
