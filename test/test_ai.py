import unittest

from suitebot.ai.game_ai import AI
from suitebot.game import game_state_factory
from suitebot.game.direction import *
from math import floor


class TestAI(unittest.TestCase):
    def setUp(self):
        self.ai = AI()

    def test_should_move_left(self):
        game_plan = ["***",
                     " 1*",
                     "***"]
        game_state = game_state_factory.create_from_game_plan_lines(game_plan)
        self.assertEqual(self.ai.make_move(1, game_state).step1, LEFT)

    def test_when_blocked_left_go_up(self):
        game_plan = ["   ",
                     "*1*",
                     "***"]
        game_state = game_state_factory.create_from_game_plan_lines(game_plan)
        self.assertEqual(self.ai.make_move(1, game_state).step1, UP)

    def test_when_blocked_up_go_right(self):
        game_plan = [" * ",
                     "*1 ",
                     "***"]
        game_state = game_state_factory.create_from_game_plan_lines(game_plan)
        self.assertEqual(self.ai.make_move(1, game_state).step1, RIGHT)

    def test_when_blocked_right_go_down(self):
        game_plan = ["***",
                     "*1*",
                     "* *"]
        game_state = game_state_factory.create_from_game_plan_lines(game_plan)
        self.assertEqual(self.ai.make_move(1, game_state).step1, DOWN)

    def test_wrap_left_move(self):
        game_plan = [" **",
                     "1**",
                     "***"]

        game_state = game_state_factory.create_from_game_plan_lines(game_plan)
        self.assertEqual(self.ai.make_move(1, game_state).step1, UP)

    def test_opponent_blocking(self):
        game_plan = [
            "   **",
            "  21*",
            "    *",
            "     ",
        ]

        game_state = game_state_factory.create_from_game_plan_lines(game_plan)
        self.assertEqual(self.ai.make_move(1, game_state).step1, DOWN)

    def test_avoid_threats(self):
        game_plan = [
            "   **",
            " 2 1*",
            "    *",
            "     ",
        ]

        game_state = game_state_factory.create_from_game_plan_lines(game_plan)
        self.assertEqual(self.ai.make_move(1, game_state).step1, DOWN)

class TestSpiral(unittest.TestCase):
    def setUp(self):
        self.ai = AI()
        self.unblocked_grid = ['     ', '     ', '  1  ', '     ', '     ']
        self.game_state = game_state_factory.create_from_game_plan_lines(self.unblocked_grid)

    def test_block(self):
        self.ai.current_direction = UP
        game_grid = ['1  ', '   ', '   ']
        game_state = game_state_factory.create_from_game_plan_lines(game_grid)
        self.assertEqual(self.ai.make_move(1, game_state).step1, RIGHT)
        self.assertEqual(self.ai.vertical_direction, UP)

    def test_block_two_move(self):
        game_grid = [' 1 ', '   ', '   ']
        game_state = game_state_factory.create_from_game_plan_lines(game_grid)
        self.ai.current_direction = RIGHT
        self.assertEqual(self.ai.make_move(1, game_state).step1, DOWN)
        self.assertEqual(self.ai.vertical_direction, DOWN)

    def test_block_low(self):
        self.ai.current_direction = DOWN
        game_grid = ['   ', '   ', '1  ']
        game_state = game_state_factory.create_from_game_plan_lines(game_grid)
        self.assertEqual(self.ai.make_move(1, game_state).step1, RIGHT)
        self.assertEqual(self.ai.current_direction, RIGHT)
        self.assertEqual(self.ai.vertical_direction, DOWN)

    def test_block_low_twomove(self):
        self.ai.current_direction = RIGHT
        self.ai.vertical_direction = DOWN
        game_grid = ['   ', '   ', ' 1 ']
        game_state = game_state_factory.create_from_game_plan_lines(game_grid)
        self.assertEqual(self.ai.make_move(1, game_state).step1, UP)
        self.assertEqual(self.ai.vertical_direction, UP)